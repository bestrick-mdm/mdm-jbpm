package com.mdm.app.process.resources;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.kie.kogito.Model;
import org.kie.kogito.auth.SecurityPolicy;
import org.kie.kogito.process.Process;
import org.kie.kogito.process.ProcessInstance;
import org.kie.kogito.process.ProcessInstances;
import org.kie.kogito.process.WorkItem;
import org.kie.kogito.services.identity.StaticIdentityProvider;

import com.mdm.app.jbpm.model.CustomerDataModel;
import com.mdm.app.jbpm.model.CustomerTask;
import com.mdm.app.jbpm.model.MapCustomerID;
import com.mdm.app.jbpm.model.ResponseStatus;

@Path("/customer/process")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CustomerJBPMProcessResource {

	private final static Logger LOGGER = Logger.getLogger(CustomerJBPMProcessResource.class.getName());

	@Inject
	@Named("mdmJBPMProcess")
	Process<? extends Model> mdmProcess;

	@POST
	@Path("/start")
	public ResponseStatus startProcess(CustomerDataModel customerDataModel) {
		ResponseStatus responseStatus = new ResponseStatus();
		try {
			LOGGER.info("Starting Process :- " + customerDataModel.toString());
			Model m = mdmProcess.createModel();
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("customerIds", customerDataModel);
			m.fromMap(parameters);
			ProcessInstance<?> processInstance = mdmProcess.createInstance(m);
			processInstance.start();
			LOGGER.info("Start Successfully :- " + processInstance.toString());
			responseStatus.setCode(200);
			responseStatus.setResponse("Successfully Completed");
		} catch (Exception e) {
			e.printStackTrace();
			responseStatus.setCode(500);
			responseStatus.setResponse("Failed");
		}
		return responseStatus;
	}

	@GET
	@Path("/list")
	public String list() {
		LOGGER.info("Starting Process :- ");
		ProcessInstances<?> processInstances = mdmProcess.instances();
		Collection<? extends ProcessInstance<?>> collection = processInstances.values();
		for (ProcessInstance<?> processInstance : collection) {
			LOGGER.info("Start Successfully :- " + processInstance.id());
			// setup security policy with identity information
			StaticIdentityProvider identity = new StaticIdentityProvider("admin",
					Collections.singletonList("managers"));
			SecurityPolicy policy = SecurityPolicy.of(identity);

			// get list of work items taking security restrictions into account
			List<WorkItem> workItems = processInstance.workItems(policy);
			LOGGER.info("Size work item  :- " + workItems.size());
			LOGGER.info("Start Successfully :- " + workItems.toString());
		}
		LOGGER.info("processInstances Successfully :- " + processInstances.toString());
		LOGGER.info("collection Successfully :- " + collection.toString());

		return "Successfully";
	}

	@GET
	@Path("/listAllTask")
	public List<CustomerTask> listAllTask() {
		List<CustomerTask> customerTasks = new ArrayList<CustomerTask>();
		ProcessInstances<?> processInstances = mdmProcess.instances();
		Collection<? extends ProcessInstance<?>> collection = processInstances.values();
		for (ProcessInstance<?> processInstance : collection) {
			LOGGER.info("Start Successfully :- " + processInstance.id());
			CustomerTask customerTask = new CustomerTask();
			customerTask.setProcessInstanceId(processInstance.id());
			// setup security policy with identity information
			StaticIdentityProvider identity = new StaticIdentityProvider("admin",
					Collections.singletonList("managers"));
			SecurityPolicy policy = SecurityPolicy.of(identity);

			// get list of work items taking security restrictions into account
			List<WorkItem> workItems = processInstance.workItems(policy);
			LOGGER.info(workItems.toString());
			Model result = (Model) processInstance.variables();
			LOGGER.info("Entry :- " + result.toMap());
			CustomerDataModel customerDataModel = (CustomerDataModel) result.toMap().get("customerIds");
			customerTask.setCustomerIds(customerDataModel.getCustomerIds());
			customerTask.setCustomerId(customerDataModel.getCustomerId());
			customerTask.setCustomerHistoryId(customerDataModel.getCustomerTableId());
			if (workItems.size() > 0)
				customerTasks.add(customerTask);
		}
		LOGGER.info("collection Successfully :- " + customerTasks.toString());
		return customerTasks;
	}

	@POST
	@Path("/completeTask")
	public ResponseStatus completeTask(MapCustomerID mapCustomerId) {
		ResponseStatus responseStatus = new ResponseStatus();
		try {
			ProcessInstances<?> processInstances = mdmProcess.instances();
			Optional<?> optional = processInstances.findById(mapCustomerId.getProcessInstanceId());
			if (optional.isPresent()) {
				ProcessInstance<?> processInstance = (ProcessInstance<?>) optional.get();
				// setup security policy with identity information
				StaticIdentityProvider identity = new StaticIdentityProvider("admin",
						Collections.singletonList("managers"));
				SecurityPolicy policy = SecurityPolicy.of(identity);

				// get list of work items taking security restrictions into account
				List<WorkItem> workItems = processInstance.workItems(policy);
				MapCustomerID mapCustomerID = new MapCustomerID();
				mapCustomerID.setCustomerId(mapCustomerId.getCustomerId());
				mapCustomerID.setMapCustomerId(mapCustomerId.getMapCustomerId());
				Map<String, Object> results = new HashMap<>();
				results.put("mapcustomerid", mapCustomerID);
				results.put("customerId", mapCustomerId.getCustomerId());
				if ("New".equalsIgnoreCase(mapCustomerId.getAction())) {
					results.put("customerMapKey", Boolean.TRUE);
				} else {
					results.put("customerMapKey", Boolean.FALSE);
				}
				LOGGER.info(results.toString());
				processInstance.completeWorkItem(workItems.get(0).getId(), results, policy);
				responseStatus.setCode(200);
				responseStatus.setResponse("Successfully Completed");
			}
		} catch (Exception e) {
			responseStatus.setCode(500);
			responseStatus.setResponse("Failed");
		}
		LOGGER.info("Successfully Completed Process");
		return responseStatus;
	}
}