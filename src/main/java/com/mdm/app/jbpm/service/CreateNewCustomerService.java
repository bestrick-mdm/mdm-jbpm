package com.mdm.app.jbpm.service;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.glassfish.jersey.client.ClientConfig;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.mdm.app.jbpm.model.CustomerDataModel;

@ApplicationScoped
public class CreateNewCustomerService {
	private final static Logger LOGGER = Logger.getLogger(CreateNewCustomerService.class.getName());
	
	
	@ConfigProperty(name = "mdm.app.url")
	String host;
	@ConfigProperty(name = "mdm.app.saveNewCustomer", defaultValue = "/customer/mapper")
	String saveNewCustomer;

	public void callCustomerMaterService(CustomerDataModel customerDataModel) {
		try {
			LOGGER.info("calling CustomerMappingService ####################### "+customerDataModel.toString());
			if (customerDataModel != null) {
				ClientConfig config = new ClientConfig();
				config.register(JacksonJsonProvider.class);
				Client client = ClientBuilder.newClient(config);
				WebTarget target = client.target(host).path(saveNewCustomer+"/"+ customerDataModel.getCustomerTableId());
				Response response = target.request(MediaType.APPLICATION_JSON).get();
				LOGGER.info("############" + response.toString());
				if (response.getStatus() == 200) {
					LOGGER.info("############ Successfully Finish CreateNewCustomerService ####################### ");
				}
			}
		} catch (Exception e) {
			LOGGER.info("Error Occur CreateNewCustomerService :- " + e.getMessage());
		}

	}
}
