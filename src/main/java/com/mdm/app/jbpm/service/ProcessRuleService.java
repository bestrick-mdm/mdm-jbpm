package com.mdm.app.jbpm.service;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.glassfish.jersey.client.ClientConfig;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.mdm.app.jbpm.model.CustomerDataModel;
import com.mdm.app.jbpm.model.ServiceResponse;

@ApplicationScoped
public class ProcessRuleService {
	private final static Logger LOGGER = Logger.getLogger(ProcessRuleService.class.getName());

	@ConfigProperty(name = "mdm.app.url")
	String host;
	@ConfigProperty(name = "mdm.app.processRuleAgain", defaultValue = "/customer/history/process/rule/")
	String processRuleAgain;

	public void processCustomerRuleAgain(CustomerDataModel customerDataModel) {
		try {
			LOGGER.info("###################### calling ProcessRuleService ####################### ");
			ClientConfig config = new ClientConfig();
			config.register(JacksonJsonProvider.class);
			Client client = ClientBuilder.newClient(config);
			WebTarget target = client.target(host).path(processRuleAgain + customerDataModel.getCustomerTableId());
			Response response = target.request(MediaType.APPLICATION_JSON).get();
			LOGGER.info("### Before #### " + response.toString());
			if (response.getStatus() == 200) {
				LOGGER.info("####### " + response.getEntity().toString());
				ServiceResponse serviceResponse = response.readEntity(ServiceResponse.class);
				LOGGER.info("### Response #### " + serviceResponse.toString());
			}
		} catch (Exception e) {
			LOGGER.info("Error Occur :- " + e.getMessage());
		}
	}
}
