package com.mdm.app.jbpm.service;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.glassfish.jersey.client.ClientConfig;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.mdm.app.jbpm.model.MapCustomerID;

@ApplicationScoped
public class CustomerMappingService {
	private final static Logger LOGGER = Logger.getLogger(CustomerMappingService.class.getName());
	
	
	@ConfigProperty(name = "mdm.app.url")
	String host;
	@ConfigProperty(name = "mdm.app.saveCustomerMapping", defaultValue = "/customer/mapper")
	String saveCustomerMapping;

	public void callCustomerMappingService(MapCustomerID mapCustomerID) {
		try {
			LOGGER.info("calling CustomerMappingService ####################### "+mapCustomerID);
			if (mapCustomerID != null) {
				ClientConfig config = new ClientConfig();
				config.register(JacksonJsonProvider.class);
				Client client = ClientBuilder.newClient(config);
				WebTarget target = client.target(host).path(saveCustomerMapping);
				Response response = target.request(MediaType.APPLICATION_JSON).post(Entity.json(mapCustomerID));
				LOGGER.info("############" + response.toString());
				if (response.getStatus() == 200) {
					LOGGER.info("############ Succesfully Finish CustomerMappingService ####################### ");
				}
			}
		} catch (Exception e) {
			LOGGER.info("Error Occur :- " + e.getMessage());
		}

	}
}
