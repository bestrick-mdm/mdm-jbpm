package com.mdm.app.jbpm.service;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;

import com.mdm.app.jbpm.model.CustomerDataModel;

@ApplicationScoped
public class MDMLoggerService {

	private final static Logger LOGGER = Logger.getLogger(MDMLoggerService.class.getName());

	public void logInformation(CustomerDataModel customerDataModel) {
		LOGGER.info("Process Data :- "+customerDataModel.toString());
	}
}
