package com.mdm.app.jbpm.model;

import java.util.ArrayList;
import java.util.List;

public class CustomerTask {

	String processInstanceId;
	List<String> customerIds = new ArrayList<String>();
	String customerId;
	Long customerHistoryId;

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public List<String> getCustomerIds() {
		return customerIds;
	}

	public void setCustomerIds(List<String> customerIds) {
		this.customerIds = customerIds;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Long getCustomerHistoryId() {
		return customerHistoryId;
	}

	public void setCustomerHistoryId(Long customerHistoryId) {
		this.customerHistoryId = customerHistoryId;
	}

}
