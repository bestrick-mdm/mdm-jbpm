package com.mdm.app.jbpm.model;

import java.util.ArrayList;
import java.util.List;

public class MapCustomerID {

	String customerId;
	String mapCustomerId;
	String processInstanceId;
	String action;
	List<String> customerIds = new ArrayList<>();

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getMapCustomerId() {
		return mapCustomerId;
	}

	public void setMapCustomerId(String mapCustomerId) {
		this.mapCustomerId = mapCustomerId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public List<String> getCustomerIds() {
		return customerIds;
	}

	public void setCustomerIds(List<String> customerIds) {
		this.customerIds = customerIds;
	}

}
