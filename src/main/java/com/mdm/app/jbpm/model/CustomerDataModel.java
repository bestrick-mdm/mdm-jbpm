package com.mdm.app.jbpm.model;

import java.util.ArrayList;
import java.util.List;

public class CustomerDataModel {
	List<String> customerIds = new ArrayList<>();
	Long customerTableId;
	String customerId;

	public List<String> getCustomerIds() {
		return customerIds;
	}

	public void setCustomerIds(List<String> customerIds) {
		this.customerIds = customerIds;
	}

	public Long getCustomerTableId() {
		return customerTableId;
	}

	public void setCustomerTableId(Long customerTableId) {
		this.customerTableId = customerTableId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return "CustomerDataModel [customerIds=" + customerIds + ", customerTableId=" + customerTableId
				+ ", customerId=" + customerId + "]";
	}

}
