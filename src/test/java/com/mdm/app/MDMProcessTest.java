package com.mdm.app;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.inject.Named;

import org.kie.kogito.Model;
import org.kie.kogito.auth.SecurityPolicy;
import org.kie.kogito.process.Process;
import org.kie.kogito.process.ProcessInstance;
import org.kie.kogito.process.WorkItem;
import org.kie.kogito.services.identity.StaticIdentityProvider;

import com.mdm.app.jbpm.model.CustomerDataModel;
import com.mdm.app.jbpm.model.MapCustomerID;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class MDMProcessTest {

	private final static Logger LOGGER = Logger.getLogger(MDMProcessTest.class.getName());
	@Inject
	@Named("mdmJBPMProcess")
	Process<? extends Model> mdmProcess;

	//@Test
	public void testMDMProcess() {

		assertNotNull(mdmProcess);
		List<String> list = new ArrayList<String>();
		list.add("14");
		list.add("15");
		CustomerDataModel dataModel = new CustomerDataModel();
		dataModel.getCustomerIds().addAll(list);
		dataModel.setCustomerId("12B");
		dataModel.setCustomerTableId(647L);
		Model m = mdmProcess.createModel();
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("customerIds", dataModel);

		m.fromMap(parameters);

		ProcessInstance<?> processInstance = mdmProcess.createInstance(m);
		processInstance.start();
		assertEquals(org.kie.api.runtime.process.ProcessInstance.STATE_ACTIVE, processInstance.status());
		LOGGER.info(processInstance.toString());
		Model result = (Model) processInstance.variables();
		assertEquals(2, result.toMap().size());
		CustomerDataModel customerDataModel = (CustomerDataModel) result.toMap().get("customerIds");
		assertNotNull(customerDataModel);
		assertEquals(list.toString(), customerDataModel.getCustomerIds().toString());

		// setup security policy with identity information
		StaticIdentityProvider identity = new StaticIdentityProvider("admin", Collections.singletonList("managers"));
		SecurityPolicy policy = SecurityPolicy.of(identity);

		// get list of work items taking security restrictions into account
		List<WorkItem> workItems = processInstance.workItems(policy);

		LOGGER.info("WorkItem  size :- " + workItems.size());
		assertEquals(1, workItems.size());
		assertEquals("MDMTask", workItems.get(0).getName());
		MapCustomerID mapCustomerID = new MapCustomerID();
		mapCustomerID.setCustomerId(customerDataModel.getCustomerId());
		mapCustomerID.setMapCustomerId(customerDataModel.getCustomerIds().get(0));

		Map<String, Object> results = new HashMap<>();
		results.put("mapcustomerid", mapCustomerID);
		

		processInstance.completeWorkItem(workItems.get(0).getId(), results, policy);

		assertEquals(org.kie.api.runtime.process.ProcessInstance.STATE_COMPLETED, processInstance.status());
	}

}
